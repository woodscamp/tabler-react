// @flow

import * as React from "react";

type Props = {|
  +children?: React.Node,

  /**
   * Logo image URL
   */
  +imageURL?: string,
|};

function StandaloneFormPage(props: Props): React.Node {
  let logo = props.imageURL ? (
    <div className="text-center mb-6">
      <img src={props.imageURL} className="h-6" alt="" />
    </div>
  ) : (
    ""
  );

  return (
    <div className="page">
      <div className="page-single">
        <div className="container">
          <div className="row">
            <div className="col col-login mx-auto">
              {logo}
              {props.children}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default StandaloneFormPage;
